﻿using OutSurance.Core;
using OutSurance.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace OutSurance.Controllers
{
    public class CSVExpoterController : Controller
    {
        private ICSVExporterManager _manager;        
        public CSVExpoterController(ICSVExporterManager manager)
        {
            _manager = manager;         
        }

        public ActionResult Index()
        {
            return View();
        }      

        // POST: CSVExpoter/Create
        [HttpPost]
        public ActionResult Read(HttpPostedFileBase upload)
        {
            try
            {
                if (ModelState.IsValid)
                {                    
                    var model = _manager.ReadFromCSV(upload);
                    var sortWithFrequency = _manager.OrderByFrequencyDescending(model);
                    var sortWithAddress = _manager.OrderByAddress(model);

                    Export(sortWithFrequency,sortWithAddress);                    

                    return View(model);
                }
                else
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
            }
            catch (Exception e)
            {
            }                                    
            
            return View();
        }  

        public void Export(IEnumerable<OrderedByFrequency> orderedbyfrequency, List<OrderedByAddress> orderedbyaddress)
        {
            // Address

            var csv = new StringBuilder();

            var newLine = string.Format("{0}", "Order Using Address");
            csv.AppendLine(newLine);

            foreach (var model in orderedbyaddress)
            {                
                newLine = string.Format("{0}", model.Address.ToString());
                csv.AppendLine(newLine);
            }

            // Frequency

            newLine = string.Format("{0}", "");
            csv.AppendLine(newLine);

            newLine = string.Format("{0}", "Order using frequency");
            csv.AppendLine(newLine);

            foreach (var model in orderedbyfrequency)
            {              

                newLine = string.Format("{0},{1}", model.LastName.ToString(), model.Frequency.ToString());
                csv.AppendLine(newLine);
            }            
            
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Output.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";
            Response.Output.Write(csv);
            Response.Flush();
            Response.End();
        }         

    }
}
