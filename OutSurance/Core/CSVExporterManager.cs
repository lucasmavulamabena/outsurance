﻿using OutSurance.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace OutSurance.Core
{
    public class CSVExporterManager : ICSVExporterManager
    {
        //public List<CSVExporterModel> manager { get; set;}
        public List<CSVExporterModel> ReadFromCSV(HttpPostedFileBase upload)
         {
            var manager = new List<CSVExporterModel>();

            if (upload != null && upload.ContentLength > 0)
            {
                if (upload.FileName.EndsWith(".csv"))
                {
                    Stream stream = upload.InputStream;
                    //...
                    using (var reader = new StreamReader(upload.InputStream))
                    {
                        string inputLine = "";
                        reader.BaseStream.Position = 0;

                        while ((inputLine = reader.ReadLine()) != null)
                        {                            
                            string[] values = inputLine.Split(new char[] { ',' });

                            var row = new CSVExporterModel
                            {
                                FirstName = values[0],
                                LastName = values[1],
                                Address = values[2],                                
                                PhoneNumber = values[3]
                            };

                            manager.Add(row);                            
                        }

                        reader.Close();            
                    }

                    return manager;
                }            
            }
            
            return manager;
        }

        public List<OrderedByAddress> OrderByAddress(List<CSVExporterModel> data)
        {
            var model = new List<OrderedByAddress>();
            int counter = 0;

            foreach (var d in data)
            {
                var val = new OrderedByAddress
                {
                   Address = d.Address,
                   OrderField = createOrderField(d.Address,counter)
                };

                counter++;
                model.Add(val);
            }

            var elements = from element in model
                           orderby element.OrderField
                           select element;

            return elements.ToList();
        }

        public string createOrderField(string address,int c)
        {
            int startIndex = address.IndexOf(" ");
            string orderfield = "";

            if (c > 0)
            {
                orderfield = address.Substring(startIndex + 1, ((address.Length - startIndex) - 1) );
            }            

            return orderfield;
        }
 
        public IEnumerable<OrderedByFrequency> OrderByFrequencyDescending(List<CSVExporterModel> data)
        {
            var model = new List<OrderedByFrequency>();
            int c = 0;

            foreach (var d in data)
            {
                var val = new OrderedByFrequency
                {
                    Frequency = getFrequency(d.FirstName,d.LastName, data),
                    LastName = d.LastName                     
                };

                if (c > 0)
                {
                    model.Add(val);
                }

                c++;
            }
            
            var elements = from element in model
                           orderby element.Frequency
                           select element;


            return elements;
        }

        public int getFrequency(string firstname,string lastname, List<CSVExporterModel> model)
        {
            int counter = 0;  
            
            foreach(var m in model)
            {
                if (m.LastName == lastname && m.FirstName == firstname)
                {
                    counter++;
                }
            }
                      
            return counter;
        }

    }
}