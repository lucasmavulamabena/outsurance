﻿using OutSurance.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OutSurance.Core
{
    public interface ICSVExporterManager
    {
        List<CSVExporterModel> ReadFromCSV(HttpPostedFileBase upload);
        IEnumerable<OrderedByFrequency> OrderByFrequencyDescending(List<CSVExporterModel> model);
        List<OrderedByAddress> OrderByAddress(List<CSVExporterModel> data);
    }
}