﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OutSurance.Models
{  
    public class CSVExporterModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }        
        public string PhoneNumber { get; set; }        
        public int Frequency { get; set; }                   
    }

    public class OrderedByFrequency
    {
        public int Frequency { get; set; }
        public string LastName { get; set; }
    }

    public class OrderedByAddress
    {
        public string Address{get; set;}
        public string OrderField { get; set; }
    }
}

   