﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OutSurance.Startup))]
namespace OutSurance
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
